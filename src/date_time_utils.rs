use chrono::offset::Utc;
use chrono::DateTime;
use std::time;

pub fn convert_to_string(t: time::SystemTime) -> Option<String> {
    let x: DateTime<Utc> = t.into();
    Some(x.to_rfc2822())
}

pub fn convert_to_datetime(t: &String) -> Option<chrono::NaiveDateTime> {
    let converted_datetime: Option<chrono::NaiveDateTime>;

    match chrono::DateTime::parse_from_rfc2822(&t) {
        Ok(v) => converted_datetime = Some(v.naive_utc()),
        Err(_e) => converted_datetime = None,
    }
    converted_datetime
}
