mod date_time_utils;
mod file_persistance_manager;
mod manager;
mod collection_utils;

fn main() -> std::io::Result<()> {
    let backup_manager = manager::BackupManager {
        dir_path: String::from("/home/woops/testdir"),
    };

    if !file_persistance_manager::backup_data_file_exists() {
        file_persistance_manager::persist_info_file(backup_manager.get_file_names()?)?;
    } else {
        let x: manager::BackupInfo = file_persistance_manager::deserialize_backup_file()?;
        backup_manager.update_records(x)?;
        file_persistance_manager::persist_info_file(backup_manager.get_file_names()?)?
    }
    Ok(())
}
