use crate::manager;
use serde::{Deserialize, Serialize};
use std::path;
use std::{fs, io};

#[derive(Serialize, Deserialize, Clone)]
struct BackupInfoJson {
    backup_dir: String,
    backup_files: Vec<BackupFileInfoJson>,
}

impl BackupInfoJson {
    pub fn new(backup_dir: String) -> BackupInfoJson {
        BackupInfoJson {
            backup_dir,
            backup_files: vec![],
        }
    }

    pub fn add_files(&mut self, files: Vec<BackupFileInfoJson>) {
        self.backup_files = files;
    }
}

#[derive(Serialize, Deserialize, Clone)]
struct BackupFileInfoJson {
    created: String,
    modified: String,
    path: String,
}

pub fn persist_info_file(backup_info: manager::BackupInfo) -> io::Result<()> {
    let mut x: BackupInfoJson = BackupInfoJson::new(backup_info.get_backup_dir());
    x.add_files(map_backup_info_to_json(&backup_info));
    fs::write(
        "backup_data.json",
        serde_json::to_string_pretty(&x).unwrap(),
    )?;
    println!("Done");
    Ok(())
}

pub fn backup_data_file_exists() -> bool {
    path::Path::new("backup_data.json").exists()
}

pub fn deserialize_backup_file() -> io::Result<manager::BackupInfo> {
    let file_contents = fs::read_to_string("backup_data.json")?;
    let backup_data: BackupInfoJson = serde_json::from_str(&file_contents)?;
    Ok(map_json_to_backup_info(&backup_data).unwrap())
}

fn map_backup_info_to_json(backup_info: &manager::BackupInfo) -> Vec<BackupFileInfoJson> {
    let mut f: Vec<BackupFileInfoJson> = vec![];

    for item in backup_info.get_files() {
        f.push(BackupFileInfoJson {
            created: item.created.clone(),
            modified: item.last_modified.clone(),
            path: item.get_path(),
        })
    }

    return f;
}

fn map_json_to_backup_info(data: &BackupInfoJson) -> io::Result<manager::BackupInfo> {
    let backup_dir: String = data.backup_dir.clone();
    let mut file_info_vec: Vec<manager::FileInfo> = vec![];

    for item in &data.backup_files {
        file_info_vec.push(manager::FileInfo {
            path: item.path.clone(),
            last_modified: item.modified.clone(),
            created: item.created.clone(),
        });
    }

    Ok(manager::BackupInfo {
        backup_dir,
        backup_files_info: file_info_vec,
    })
}
