extern crate chrono;
use crate::collection_utils::flatten_vector;
use crate::date_time_utils;
use std::path::Path;
use std::sync::mpsc::{Receiver, Sender};
use std::sync::{mpsc, Arc};
use std::{fs, io, thread, time};

pub struct BackupManager {
    pub dir_path: String,
}

pub struct BackupInfo {
    pub backup_dir: String,
    pub backup_files_info: Vec<FileInfo>,
}

pub struct FileInfo {
    pub path: String,
    pub last_modified: String,
    pub created: String,
}

impl PartialEq for FileInfo {
    fn eq(&self, other: &Self) -> bool {
        date_time_utils::convert_to_datetime(&self.last_modified)
            .unwrap()
            .eq(&date_time_utils::convert_to_datetime(&other.last_modified).unwrap())
    }
}

struct FileModificationInfo {
    last_modified: time::SystemTime,
    created: time::SystemTime,
}

impl BackupInfo {
    pub fn new(bd: String) -> BackupInfo {
        BackupInfo {
            backup_dir: bd,
            backup_files_info: vec![],
        }
    }

    pub fn set_backup_files_info(&mut self, file_info_list: Vec<FileInfo>) -> &Self {
        self.backup_files_info = file_info_list;
        self
    }

    pub fn get_files(&self) -> &Vec<FileInfo> {
        &self.backup_files_info
    }

    pub fn get_backup_dir(&self) -> String {
        self.backup_dir.clone()
    }
}

impl BackupManager {
    pub fn get_file_names(&self) -> std::io::Result<BackupInfo> {
        let _path = Path::new(&self.dir_path);

        let mut file_data: Vec<FileInfo> = Vec::new();
        let mut backup_info: BackupInfo = BackupInfo::new(self.dir_path.clone());

        if _path.exists() && _path.is_dir() {
            scan_directory(_path, &mut file_data)?;

            backup_info.set_backup_files_info(file_data);
        }
        Ok(backup_info)
    }

    pub fn update_records(&self, mut records: BackupInfo) -> io::Result<BackupInfo> {
        let mut current_files: Vec<FileInfo> = vec![];
        scan_directory(Path::new(&self.dir_path), &mut current_files)?;
        let x = records.backup_files_info.clone();

        let updated_records: Vec<FileInfo> = compare_records(&x, &current_files);
        records.set_backup_files_info(updated_records);
        Ok(records)
    }
}

impl FileInfo {
    pub fn get_path(&self) -> String {
        self.path.clone()
    }
}

impl Clone for FileInfo {
    fn clone(&self) -> Self {
        FileInfo {
            path: self.path.clone(),
            last_modified: self.last_modified.clone(),
            created: self.created.clone(),
        }
    }
}

fn scan_directory<'a>(
    path: &'a Path,
    file_data: &'a mut Vec<FileInfo>,
) -> io::Result<&'a Vec<FileInfo>> {
    for entry in fs::read_dir(path)? {
        let entry = entry?;
        if entry.path().is_file() {
            if let Ok(mod_info) = get_file_mod_info(&entry.path()) {
                file_data.push(FileInfo {
                    path: entry.path().to_str().unwrap().to_string(),
                    last_modified: date_time_utils::convert_to_string(mod_info.last_modified)
                        .unwrap(),
                    created: date_time_utils::convert_to_string(mod_info.created).unwrap(),
                });
            }
        } else {
            scan_directory(&entry.path(), file_data)?;
        }
    }

    Ok(file_data)
}

fn get_file_mod_info(path: &Path) -> io::Result<FileModificationInfo> {
    let metadata = path.metadata()?;
    let mod_info: FileModificationInfo = FileModificationInfo {
        last_modified: metadata.created()?,
        created: metadata.accessed()?,
    };

    Ok(mod_info)
}

fn compare_records(
    existing_records: &Vec<FileInfo>,
    current_records: &Vec<FileInfo>,
) -> Vec<FileInfo> {
    let (tx, rx): (Sender<Vec<FileInfo>>, Receiver<Vec<FileInfo>>) = mpsc::channel();

    let mut children = vec![];

    for id in 0..4 {
        let existing_records_arc = Arc::new(existing_records.clone());
        let current_records_arc = Arc::new(current_records.clone());

        let thread_tx = tx.clone();

        let child = thread::spawn(move || {
            let changed_files = get_changed_files(
                existing_records_arc.clone(),
                current_records_arc.clone(),
                1,
                id,
            );

            thread_tx.send(changed_files).unwrap();
        });

        children.push(child);
    }

    let mut changed_files_from_all_threads: Vec<Vec<FileInfo>> = Vec::with_capacity(4 as usize);
    for _ in 0..4 {
        match rx.recv() {
            Ok(v) => changed_files_from_all_threads.push(v),
            Err(_) => {}
        }
    }

    for child in children {
        child.join().expect("oops! the child thread panicked");
    }

    let mut mut_current_records = current_records.clone();
    let mut all_changed_records_from_all_dirs = flatten_vector(changed_files_from_all_threads);

    if all_changed_records_from_all_dirs.len() > 0 {
        println!("Updating db file with new records");
        update_with_changed_records(
            &mut mut_current_records,
            &mut all_changed_records_from_all_dirs,
        );
    } else {
        println!("No updated files");
    }


    mut_current_records.clone()
}

fn update_with_changed_records(current: &mut Vec<FileInfo>, changed: &mut Vec<FileInfo>) {
    for item in changed {
        let index = current.iter().position(|x| x.path == item.path).unwrap();
        current.remove(index);
        current.push(item.clone());
    }
}
fn get_changed_files(
    existing_records: Arc<Vec<FileInfo>>,
    current_records: Arc<Vec<FileInfo>>,
    chunk_size: usize,
    thread_index: usize,
) -> Vec<FileInfo> {
    let mut changed_records: Vec<FileInfo> = vec![];

    let start = thread_index;
    let stop = thread_index + chunk_size;
    let slice1 = &existing_records[start..stop];
    let slice2 = &current_records[start..stop];

    for (existing_file_info, current_file_info) in slice1.iter().zip(slice2.iter()) {
        if !existing_file_info.eq(current_file_info) {
            changed_records.push(current_file_info.clone());
        }
    }
    changed_records
}
